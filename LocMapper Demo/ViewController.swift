//
//  ViewController.swift
//  LocMapper Demo
//
//  Created by François Lamboley on 10/10/2019.
//  Copyright © 2019 happn. All rights reserved.
//

import Cocoa
import XibLoc



class ViewController: NSViewController {
	
	@IBOutlet var titleLabel: NSTextField!
	@IBOutlet var connectedUsersLabel: NSTextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		XibLoc.di.defaultPluralityDefinition = PluralityDefinition(string: NSLocalizedString("default plurality definition", value: "(1)", comment: "The default plurality definition for your language. See the dev for explanations."))
		
//		titleLabel.stringValue = NSLocalizedString("hello world", comment: "Title of the view")
		connectedUsersLabel.stringValue = NSLocalizedString("n connected users", comment: "Title of the view").applying(
			xibLocInfo: XibLocResolvingInfo(replacement: "", pluralValue: NumberAndFormat(2))
		)
	}
	
}
