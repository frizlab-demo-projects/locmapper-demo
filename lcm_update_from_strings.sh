#!/bin/bash

input_filename=${1:-Localization Map.lcm}
csv_separator=${2:-,}

if [ ! -f "$input_filename" ]; then
	echo "$0: $input_filename: no such file" >/dev/stderr
	exit 1
fi

LOC_MAPPER="locmapper"

"$LOC_MAPPER" merge_xcode_locs --csv-separator="$csv_separator" --exclude-list="Carthage/,Dependencies/,.git/" --include-list="/en.lproj/" ./ "$input_filename" \
	en.lproj      " English"                                   \
	fr.lproj      "Français — French"
