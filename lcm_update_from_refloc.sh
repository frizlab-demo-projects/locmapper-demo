#!/bin/bash

readonly DEFAULT_PROJECT_ID="REDACTED"

project_id=${1:-$DEFAULT_PROJECT_ID}
input_filename=${2:-Localization Map.lcm}
csv_separator=${3:-,}

if [ ! -f "$input_filename" ]; then
	echo "$0: $input_filename: no such file" >/dev/stderr
	exit 1
fi

merge_style=add
if [ "$project_id" = "$DEFAULT_PROJECT_ID" ]; then
	merge_style=replace
fi

LOC_MAPPER="locmapper"

"$LOC_MAPPER" merge_lokalise_trads_as_xibrefloc --merge-style="$merge_style" "REDACTED" "$project_id" "$input_filename" \
	en         " English"                                   \
	fr         "Français — French"
