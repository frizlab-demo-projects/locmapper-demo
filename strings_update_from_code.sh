#!/bin/bash


# Basic Config
utf8=1; # Default is utf16
do_xibs=1
do_strings=1
unused_files_path="$(pwd)/.localizesh__unused_files"
unlocalized_xibs_path="$(pwd)/.localizesh__unlocalized_xibs"
folders=("LocMapper Demo")



# Checking whether Python 3 is installed (merge_strings.py is now in Python 3)
if ! which -s python3; then
	echo_error "Python 3 is required for this script to run (brew install python@3)" >/dev/stderr
	exit 42
fi



# UTF-8 option
merge_strings_utf_options=
if [ $utf8 -eq 1 ]; then merge_strings_utf_options="-utf8"; fi

################################################################################
# xib/storyboard localization

function do_xibs_from_pwd() {
	find . -name "*.xib" -o -name "*.storyboard" | while read f; do
		parent_dirname="$(basename "$(dirname "$f")")"
		if [ "$parent_dirname" != "Base.lproj" ]; then
			if ! grep -qE "^$f\$" "$unlocalized_xibs_path" 2>/dev/null; then
				echo_warning "*** Warning: Unlocalized xib or storyboard (add to "$unlocalized_xibs_path" to remove this warning): $f"
			fi
			continue
		fi

		bname="$(basename "$f")"
		bname_no_ext="$(echo "$bname" | sed -E 's/\.(xib|storyboard)$//g')"
		bname_strings="$bname_no_ext".strings

		bdirname="$(dirname "$f")/.."
		if [ ! -e "$bdirname/Base.lproj/$bname" ]; then
			echo_error "***** SERIOUS ERROR: Internal check failed. Stopping everything (currently localizing xib/storyboard; the rest is untouched, but YOU SHOULD CLEAN MANUALLY what's been done)" >/dev/stderr
			return 7
		fi
		for language in "$bdirname"/en.lproj; do
			if [ ! -d "$language" ]; then continue; fi; # Eg. if there are no .lproj folder

			language="$(basename "$language")"
			if [ "$language" = "Base.lproj" ]; then continue; fi

			new="$bdirname/$language/$bname_strings"
			old="$bdirname/$language/$bname_strings".old
			if [ -e "$old" ]; then
				echo_warning "*** Warning: Cannot localize $f for language $language: $old already exists." >/dev/stderr
				continue
			fi

			if [ -f "$new" ]; then mv "$new" "$old"; fi
			ibtool --export-strings-file "$new.utf16" "$bdirname/Base.lproj/$bname"
			if [ $utf8 -eq 1 ]; then iconv -f "UTF-16" -t "UTF-8" "$new.utf16" >"$new" && rm "$new.utf16"; fi
			if [ -f "$old" ]; then
				echo "Merging file $(echo $new | sed 's:/Base.lproj/..::g')"
				printf "$COLOR_WARNING"
				"../merge_strings.py" $merge_strings_utf_options "$old" "$new" "$new"
				ret=$?
				printf "$COLOR_CLEAR"
				if [ $ret -eq 0 ]; then rm "$old"
				else
					echo_error "***** File "$new" was not successfully merged. It has been left at its original version." >/dev/stderr
					mv "$old" "$new"
				fi
			fi
		done
	done
}


################################################################################
# Localization of the rest

# Utility function
function patch_table_name() {
	file="$1"
	old_file="$file".loctmp
	if [ -e "$old_file" ]; then
		echo_error "Error: File \"$f\" already have a loctmp." >/dev/stderr
		return 1
	fi
	mv "$file" "$old_file"
	base_ext="$(basename "$file")"
	base="$(basename "$base_ext" .swift)"
	if [ "$base" = "$base_ext" ]; then base="$(basename "$base_ext" .m)"; fi
	if [ "$base" = "$base_ext" ]; then base="$(basename "$base_ext" .c)"; fi
	base="$(echo "$base" | sed -e 's/\+[^+]*$//g')"
	#echo_debug "$file --> $base"
	# The three first regexp are to convert Swift-styled localized string to ObjC-style localized strings.
	# Constraints:
	#    - The keys and comments must be double-quoted constant strings and not contain double-quotes;
	#    - The table name must either be one of the pre-defined tables or a string with the same constraints as above;
	#    - The call to the NSLocalizedString function must be on one line.
	# sed note: Apple's sed does not handle \t... so they are replaced by actual tabulations :(
	sed -E -e 's/NSLocalizedString\([ 	]*("[^"]*")[ 	]*,[ 	]*comment[ 	]*:[ 	]*("[^"]*")[ 	]*\)/NSLocalizedString(@\1, @\2)/g' \
			 -e 's/NSLocalizedString\([ 	]*("[^"]*")[ 	]*,[ 	]*tableName[ 	]*:[ 	]*([^",]*),[ 	]*comment[ 	]*:[ 	]*("[^"]*")[ 	]*\)/NSLocalizedStringFromTable(@\1, \2, @\3)/g' \
			 -e 's/NSLocalizedString\([ 	]*("[^"]*")[ 	]*,[ 	]*tableName[ 	]*:[ 	]*("[^"]*"),[ 	]*comment[ 	]*:[ 	]*("[^"]*")[ 	]*\)/NSLocalizedStringFromTable(@\1, @\2, @\3)/g' \
			 -e "s/HC_LT_LOGIN/\@\"HC Login\"/g" \
			 -e "s/HC_LT_TUTORIAL/\@\"HC Tutorial\"/g" \
			 -e "s/HC_LT_HOME/\@\"HC Home\"/g" \
			 -e "s/HC_LT_USER_PROFILES/\@\"HC User Profiles\"/g" \
			 -e "s/HC_LT_NOTIFICATIONS/\@\"HC Notifications\"/g" \
			 -e "s/HC_LT_CONVERSATIONS/\@\"HC Conversations\"/g" \
			 -e "s/HC_LT_PHOTOS/\@\"HC Photos\"/g" \
			 -e "s/HC_LT_PREFERENCES/\@\"HC Preferences\"/g" \
			 -e "s/HC_LT_POP_UPS/\@\"HC Pop-Ups\"/g" \
			 -e "s/HC_LT_INVITE_FRIENDS/\@\"InviteFriends\"/g" \
			 -e "s/HC_LT_ERRORS/\@\"HC Errors\"/g" \
			 -e "s/HC_LT_INFOS/\@\"HC Info\"/g" \
			 -e "s/HC_LT_MAP/\@\"HC Map\"/g" \
			 -e "s/HC_SELF_CLASS_STR/\@\"$base\"/g" \
			 -e "s/HC_CLASS_STR/\@\"$base\"/g" \
			 -e "s/HC_LT_MAP/\@\"HC Map\"/g" \
			 -e "s/HC_LT_COUNTRY/\@\"Country\"/g" \
			 -e "s/HC_LT_GAME/\@\"Game\"/g" \
		"$old_file" >"$file"
}

function localize_the_rest_from_pwd() {
	# First safety check. If there's a loctmp file in the project dir, we refuse running.
	find . -name "*.loctmp" | while read f; do
		echo_error "loctmp file found in repository!" >/dev/stderr
		echo_error "***** Localization cancelled. YOU SHOULD MANUALLY CLEAN THE REPOSITORY *****" >/dev/stderr
		return 152
	done
	ret=$?
	if [ $ret -ne 0 ]; then return $ret; fi

	# Let's patch the table name of all the source files
	find . -name "*.c" -o -name "*.m" -o -name "*.swift" | while read f; do
		patch_table_name "$f"
		if [ $? -ne 0 ]; then
			echo_error "***** Localization cancelled. YOU SHOULD MANUALLY CLEAN THE REPOSITORY *****" >/dev/stderr
			return 150
		fi
	done
	ret=$?
	if [ $ret -ne 0 ]; then return $ret; fi

	# Let's generate and merge the strings files
	for language_folder in Localizables/en.lproj; do
		if [ ! -d "$language_folder" ]; then continue; fi; # Eg. if there are no .lproj folder

		language="$(basename "$language_folder")"
		if [ "$language" = "Base.lproj" ]; then continue; fi

		# Renaming existing .strings in .strings.old
		for f in "$language_folder"/*.strings; do
			if [ ! -f "$f" ]; then continue; fi; # Eg. There are no .strings in the lproj folder
			if [ -e "$f".old ]; then
				echo_error "***** LANGUAGE $language HAS BEEN SKIPPED AND MUST BE MANUALLY CLEANED (file $f.old already existed) *****" >/dev/stderr
				return 1
			fi
			mv "$f" "$f".old
		done
		ret=$?
		if [ $ret -ne 0 ]; then continue; fi

		# Generating new .strings
		find . \( -name "*.c" -o -name "*.m" -o -name "*.swift" \) -print0 | xargs -0 genstrings -q -o "$language_folder"
		if [ $utf8 -eq 1 ]; then
			# Convert generated strings to UTF-8...
			for f in "$language_folder"/*.strings; do
				iconv -f "UTF-16" -t "UTF-8" "$f" >"$f.utf8" && mv "$f.utf8" "$f"
			done
		fi

		# Merging .strings with old ones, and removing the old strings
		for f in "$language_folder"/*.strings.old; do
			if [ ! -f "$f" ]; then continue; fi; # Eg. There were no .strings in the lproj folder
			new="$language_folder/$(basename "$f" .old)"
			if [ -f "$new" ]; then
				echo "Merging file $new"
				printf "$COLOR_WARNING"
				"../merge_strings.py" $merge_strings_utf_options "$f" "$new" "$new"
				ret=$?
				printf "$COLOR_CLEAR"
				if [ $ret -eq 0 ]; then rm "$f"
				else
					echo_error "***** File "$new" was not successfully merged. It has been left at its original version." >/dev/stderr
					mv "$f" "$new"
				fi
			else
				mv "$f" "$new"
				if ! grep -qE "^$new\$" "$unused_files_path" 2>/dev/null; then
					echo_warning "*** Warning: Got seemingly unused file (consider removing, or add to $unused_files_path to remove warning)): $new"
				fi
			fi
		done
	done

	# Now let's move the loctmp files back to their original name
	find . -name "*.loctmp" | while read f; do
		nf="$(dirname "$f")/$(basename "$f" .loctmp)"
		mv "$f" "$nf"
		if [ $? -ne 0 ]; then
			echo_error "*** Cannot move back a loctmp file. Localization done, but YOU SHOULD MANUALLY CLEAN THE REPOSITORY." >/dev/stderr
			return 151
		fi
	done
	ret=$?
	if [ $ret -ne 0 ]; then return $ret; fi
}



base_dir="$(pwd)"
for folder in "${folders[@]}"; do
	# We assume echo_error won't ever fail.
	cd "$folder"
	if [ $? -ne 0 ]; then echo_error "Cannot cd to folder \"$folder\". Skipping..." >/dev/stderr; continue; fi
	echo
	echo
	echo
	echo "********************* LOCALIZING $folder *********************"
	echo
	echo
	if [ $do_xibs -eq 1 ]; then
		echo "Localizing storyboard and xib files"
		echo
		do_xibs_from_pwd; ret=$?
		if [ $ret -ne 0 ]; then exit $ret; fi
	else
		echo_info "Skipping localization of storyboard and xib files"
	fi
	echo
	if [ $do_strings -eq 1 ]; then
		echo "Localizing the rest"
		echo
		localize_the_rest_from_pwd; ret=$?
		if [ $ret -ne 0 ]; then exit $ret; fi
	else
		echo "Skipping localization of the rest"
	fi
	cd "$base_dir"
done
